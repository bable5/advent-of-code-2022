package com.gitlab.bable5.aoc2022

import com.gitlab.bable5.aocutil.Reader
import java.lang.RuntimeException

interface Exp {
    fun value(): Long
}

data class Literal(val v: Long): Exp {
    override fun value(): Long = v
}

data class BinOp(val lhs: String, val rhs: String, val op: (Long, Long) -> Long): Exp {
    private var computedValue: Long? = null
    override fun value(): Long {
        if (this.computedValue == null) {
            val lhsV = exprs[lhs]!!.value()
            val rhsV = exprs[rhs]!!.value()
            op(lhsV, rhsV).also { this.computedValue = it }
        } else{
            println("Using memoized value")
        }
        return this.computedValue!!
    }
}

private val exprs = HashMap<String, Exp>()

fun main() {
    val input = Reader().readResource("/2022/day_21.txt")
    input.map{parseDay21(it)}.fold(exprs) { acc, v ->
        acc[v.first] = v.second
        acc
    }

    println(exprs["root"]!!.value())

}

fun parseDay21(s: String): Pair<String, Exp> {
    val parts = s.split(":")
    val name = parts[0]
    val expLit = parts[1].trim()
    var rest: Exp
//    println(expLit)
    if("""[0-9]+""".toRegex().matches(expLit)) {
        rest = Literal(expLit.toLong())
    } else {
        val (n1, op, n2) = """^([a-z]+)\s(.)\s([a-z]+)$""".toRegex().find(expLit)!!.destructured
        rest = when(op) {
            in "+" -> BinOp(n1, n2) { lhs: Long, rhs: Long -> lhs + rhs }
            in "-" -> BinOp(n1, n2) { lhs: Long, rhs: Long -> lhs - rhs }
            in "/" -> BinOp(n1, n2) { lhs: Long, rhs: Long -> lhs / rhs }
            in "*" -> BinOp(n1, n2) { lhs: Long, rhs: Long -> lhs * rhs }
            else -> throw RuntimeException("Unknown operator $op")
        }
    }

    return Pair(name, rest)
}

