package com.gitlab.bable5.aoc2022

import com.gitlab.bable5.aocutil.Reader

fun main(args: Array<String>) {
    val readResource = Reader().readResource("/2022/day_01.txt")
    val grouped = preprocess(readResource)

    val top3 = grouped.map { g -> g.sum() }
        .sortedDescending()
        .take(3)
        .sum()

    println(top3)

}

fun preprocess(input: List<String>): List<List<Int>> {
    var output = ArrayList<List<Int>>()
    var partition = ArrayList<Int>()
    for(i in input) {
        val trimmed = i.trim()
        if(trimmed.isEmpty()) {
            output.add(partition)
            partition = ArrayList()
            continue
        }
        partition.add(trimmed.toInt())
    }
    return output
}
