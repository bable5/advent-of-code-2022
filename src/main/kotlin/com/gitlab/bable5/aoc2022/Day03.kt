package com.gitlab.bable5.aoc2022

import com.gitlab.bable5.aocutil.Reader

fun main() {
    val input = Reader().readResource("/2022/day_03.txt")
    val part1 = input
        .map { splitInHalf(it) }
        .map { findCommonLetter(it) }
        .sumOf { toPriorityValue(it) }

    println("Part1: $part1")

    // Part 2
    val part2 = input.windowed(3, 3)
        .map{ findCommonLetter(it) }
        .sumOf{ toPriorityValue(it) }

    println("Part2: $part2")
}

fun splitInHalf(input: String): List<String> {
    val len = input.length / 2;
    return listOf(
        input.substring(0 until len),
        input.substring(len)
    )
}

fun findCommonLetter(lls: List<String>): Char {
    val common = lls.slice(1 until lls.size).fold(lls[0].toSet()){ acc, v ->
        acc.intersect(v.toSet())
    }

    if(common.size != 1) {
        throw IllegalArgumentException("Found more than one common character between $lls")
    }

    return common.first()
}

fun toPriorityValue(c: Char): Int = when (c) {
    in 'a'..'z' -> {
        c - 'a' + 1
    }
    in 'A' .. 'Z' -> {
        c - 'A' + 27
    }
    else -> {
        throw IllegalArgumentException("Unexpected character '$c'")
    }
}

fun <T> groupsOf(items: List<T>, size: Int): List<List<T>> {
    val itemCount = items.size
    if(itemCount % size != 0) {
        throw IllegalArgumentException("List of size $itemCount cannot be evenly grouped into $size")
    }

    val result = ArrayList<List<T>>(itemCount / size) // We know how big the list needs to be at the start.
    for(i in 0 until itemCount step size) {
        val next = items.subList(i, i + size)
        result.add(next)
    }
    return result
}

