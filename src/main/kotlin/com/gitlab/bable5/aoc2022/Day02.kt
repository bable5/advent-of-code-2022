package com.gitlab.bable5.aoc2022

import com.gitlab.bable5.aocutil.Reader

fun main() {
//    val inputFile = "/2022/day_02_example.txt"
    val inputFile = "/2022/day_02.txt"
    val input = Reader().readResource(inputFile)
        .map { l -> l.split(" ").map { elem -> elem.trim() } }

    val score = computeScorePart1(input)
    println(score)

    val score2 = computeScorePart2(input)
    println(score2)
}

fun decode(s: String): String = when (s) {
    "A", "X" -> "R"
    "B", "Y" -> "P"
    "C", "Z" -> "S"
    else -> throw IllegalArgumentException("Unknown input '$s'")
}


fun scorePlay(v: String) = when (v) {
    "R" -> 1
    "P" -> 2
    "S" -> 3
    else -> throw IllegalArgumentException("Unknown play '$v'")
}

fun scoreRound(p1: String, p2: String): Long {
    return if (p1 == p2) {
        3
    } else if (p2 == "R" && p1 == "S") {  // P2 played rock, P1 Scissors
        6
    } else if (p2 == "P" && p1 == "R") { // P2 played paper, P1 Rock
        6
    } else if (p2 == "S" && p1 == "P") { // P2 played scissors, P1 paper
        6
    } else {
        0
    }
}

fun chooseMove(p1Play: String, strategy: String): String = when (strategy) {
    "X" -> when (p1Play) { // lose
        "R" -> "S"
        "P" -> "R"
        "S" -> "P"
        else -> throw IllegalArgumentException("Unknown play '$p1Play'")
    }

    "Y" -> when (p1Play) { // draw
        "R" -> "R"
        "P" -> "P"
        "S" -> "S"
        else -> throw IllegalArgumentException("Unknown play '$p1Play'")
    }

    "Z" -> when (p1Play) { // win
        "R" -> "P"
        "P" -> "S"
        "S" -> "R"
        else -> throw IllegalArgumentException("Unknown play '$p1Play'")
    }

    else -> throw IllegalArgumentException("Unknown strategy '${strategy}")
}


fun computeScorePart1(input: List<List<String>>): Long = input.fold(0L) { acc, v ->
    val p1 = decode(v[0])
    val p2 = decode(v[1])
    val roundScore = scoreRound(p1, p2)
    val playScore = scorePlay(p2)
    acc + roundScore + playScore
}

fun computeScorePart2(input: List<List<String>>): Long = input.fold(0L) { acc, v ->
    val p1 = decode(v[0])
    val p2 = chooseMove(p1, v[1])
    val roundScore = scoreRound(p1, p2)
    val playScore = scorePlay(p2)
    acc + roundScore + playScore
}