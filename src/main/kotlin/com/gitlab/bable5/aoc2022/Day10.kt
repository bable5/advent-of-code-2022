package com.gitlab.bable5.aoc2022

import com.gitlab.bable5.aocutil.Reader

interface Op{
    val cycles: Int
}

val Noop = object : Op {
    override val cycles: Int = 1
}

data class Addx(val immediate: Int): Op {
    override val cycles: Int = 2
}

data class SimpleAddX(val immediate: Int): Op {
    override val cycles: Int = 1
}

data class ALU(val pc: Int, val x: Int) {
    fun eval(op: Op): ALU = when (op) {
        Noop -> this.copy(pc = pc + op.cycles)
        is SimpleAddX -> this.copy(pc = pc + op.cycles, x = x + op.immediate)
        else -> throw IllegalArgumentException("Unknown op $op")
    }
}

fun <T> trace(t: T): T {
    println(t)
    return t
}

val cyclesToInspect = setOf(
    20, 60, 100, 140, 180, 220
)

fun main() {
    val input = Reader().readResource("/2022/day_10.txt")
    var sum = 0
    val instrs = input.map { parse(it) }
        .flatMap { simplifyInstruction(it) }
    instrs
        .fold(ALU(1, 1)){acc, v ->
            val next = acc.eval(v)
            if(cyclesToInspect.contains(next.pc)) {
                val v = next.pc * next.x
                sum += v
                println("$next: v: $v, sum: $sum")

            }
            next
        }
    println(sum)

    renderPart2(instrs)
}

fun simplifyInstruction(op: Op): List<Op> = when(op) {
    is Addx -> listOf(SimpleAddX(0), SimpleAddX(op.immediate))
    else -> listOf(op)
}

fun parse(input: String): Op {
    val parts = input.split(" ")
    return when (parts[0]) {
        "noop" -> Noop
        "addx" -> Addx(parts[1].toInt())
        else -> throw IllegalArgumentException("Unknown value ${parts[0]}")
    }
}

fun renderPart2(input: List<Op>) {
    var alu = ALU(1, 1)
    for(hline in 0 .. 40) {
        for(vline in 0 .. 6) {
            //
        }
    }

}

fun blankScreen(): Array<Boolean> = Array(6 * 40) { false }

fun blit(screen: Array<Boolean>, vline: Int, hline: Int, v: Boolean) {
    val pos = vline * 40 + hline
    screen[pos] = v
}

