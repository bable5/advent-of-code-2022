package com.gitlab.bable5.aoc2022

import com.gitlab.bable5.aocutil.Reader
import java.lang.RuntimeException

fun main() {
    val input = Reader().readResource("/2022/day_06.txt")[0]

    println(startOfPacketMarker(input, 4))
    println(startOfPacketMarker(input, 14))
}

fun startOfPacketMarker(input: String, stride: Int): Any {
    val chars = input.toCharArray()

    for(i in 0 until chars.size - 4) {
        val window = input.slice(i until i + stride)
        val chars = window.toSet()

        if(chars.size == stride) {
            return i + stride
        }
    }
    throw RuntimeException("Did not find the answer")
}

