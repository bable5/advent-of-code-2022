package com.gitlab.bable5.aoc2022

import com.gitlab.bable5.aocutil.Reader

fun main() {
    val input = Reader().readResource("/2022/day_04.txt")
        .map { it.split(",") }
        .map{v -> Pair(inputToPair(v[0]), inputToPair(v[1]))}

    println(input.count { overlapAll(it) })
    println(input.count { overlapAny(it.first, it.second)})
}

fun overlapAll(elem: Pair<Pair<Int, Int>, Pair<Int, Int>>): Boolean {

    return completelyContained(elem.first, elem.second) || completelyContained(elem.second, elem.first)
}

fun overlapAny(r1: Pair<Int, Int>, r2: Pair<Int, Int>): Boolean {

    return     inRange(r1.first, r2) || inRange(r1.second, r2)
            || inRange(r2.first, r1) || inRange(r2.second, r1)
}

fun inRange(v: Int, r: Pair<Int, Int>): Boolean = v in r.first..r.second


fun completelyContained(p1: Pair<Int, Int>, p2: Pair<Int, Int>): Boolean = p1.first <= p2.first && p1.second >= p2.second

fun inputToPair(v: String): Pair<Int, Int> {
    val elems = v.split("-")
    return Pair(elems[0].toInt(), elems[1].toInt())
}