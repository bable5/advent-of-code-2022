package com.gitlab.bable5.aoc2022

import com.gitlab.bable5.aocutil.Reader

/* GRAMMAR

$ cd $DIR_NAME
$ ls
dir $DIR_NAME
$SIZE $FILE_NAME
 */

operator fun Regex.contains(text: CharSequence): Boolean = this.matches(text)

val CD = """^\$ cd (.+)""".toRegex()
val LS = """^\$ ls""".toRegex()
val DIR = """dir (.+)""".toRegex()
val FILE = """([0-9]+) (.+)""".toRegex()

fun thing(instr: String) {
    when(instr) {
        in Regex("\$ cd *") -> {
            println("CD '$instr'")
        }
        in Regex("\$ ls") -> println("LS '$instr'")
        in Regex("dir ") -> TODO()
    }
}

fun main() {
    val input = Reader().readResource("/2022/day_07_example.txt")
    input.forEach{thing(it)}
}




