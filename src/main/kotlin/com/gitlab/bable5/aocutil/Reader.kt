package com.gitlab.bable5.aocutil


class Reader {
    fun readResource(path: String) =
            this::class.java
                .getResourceAsStream(path)
                .bufferedReader()
                .readLines()
}