package com.gitlab.bable5.aoc2022

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class Day06KtTest {

    @Test
    fun example1() {
        val input = "mjqjpqmgbljsphdztnvjfqwrcgsmlb"
        val actual = startOfPacketMarker(input, 4)

        assertEquals(7, actual)
    }

    @Test
    fun example2() {
        assertEquals(5, startOfPacketMarker("bvwbjplbgvbhsrlpgdmjqwftvncz", 4))
        assertEquals(6, startOfPacketMarker("nppdvjthqldpwncqszvftbrmjlhg", 4))
        assertEquals(10, startOfPacketMarker("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", 4))
        assertEquals(11, startOfPacketMarker("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", 4))
    }

    @Test
    fun part2Example() {
        assertEquals(19, startOfPacketMarker("mjqjpqmgbljsphdztnvjfqwrcgsmlb", 14))
        assertEquals(23, startOfPacketMarker("bvwbjplbgvbhsrlpgdmjqwftvncz", 14))
        assertEquals(23, startOfPacketMarker("nppdvjthqldpwncqszvftbrmjlhg", 14))
        assertEquals(29, startOfPacketMarker("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", 14))
        assertEquals(26, startOfPacketMarker("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", 14))
    }
}