package com.gitlab.bable5.aoc2022

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class Day10KtTest {
    @Test
    fun parseNoop() {
        val parsed = parse("noop")
        assertSame(Noop, parsed)
    }

    @Test
    fun parseAddX() {
        val parsed = parse("addx 3")
        assertEquals(Addx(3), parsed)
    }
}