package com.gitlab.bable5.aoc2022

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class Day07KtTest {
    @Test
    fun testCD() {
        assertTrue(CD.matches("$ cd /"))
        val rootMatch = CD.find("$ cd /")!!

        val (dirName) = rootMatch.destructured
        assertEquals("/", dirName)
    }

    @Test
    fun testLS() {
        assertTrue(LS.matches("$ ls"))
    }

    @Test
    fun testDIR() {
        val moveBack = "dir .."
        val moveIn = "dir a"
        assertTrue(DIR.matches(moveBack))
        assertTrue(DIR.matches(moveIn))
    }

    @Test
    fun testFile() {
        val file = "12345 f.txt"
        val (size, name) = FILE.find(file)!!.destructured

        assertEquals("12345", size)
        assertEquals("f.txt", name)
    }
}