package com.gitlab.bable5.aoc2022

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class Day04KtTest {

    @Test
    fun part2example1() {
        assertFalse(overlapAny(Pair(2,4), Pair(6,8)))
    }

    @Test
    fun part2ExampleOverlap1() {
        assertTrue(overlapAny(Pair(2,8), Pair(3,7)))
    }
}