package com.gitlab.bable5.aoc2022

import com.gitlab.bable5.aocutil.Reader
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class Day03KtTest {

    @Test
    fun splitExamples() {
        assertEquals(listOf("vJrwpWtwJgWr", "hcsFMMfFFhFp"), splitInHalf("vJrwpWtwJgWrhcsFMMfFFhFp"))
        assertEquals(listOf("jqHRNqRjqzjGDLGL", "rsFMfFZSrLrFZsSL"), splitInHalf("jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL"))
    }

    @Test
    fun instersectExamples() {
        assertEquals('p', findCommonLetter(listOf("vJrwpWtwJgWr", "hcsFMMfFFhFp")))
        assertEquals('L', findCommonLetter(listOf("jqHRNqRjqzjGDLGL", "rsFMfFZSrLrFZsSL")))
        assertEquals('s', findCommonLetter(splitInHalf("CrZsJsPPZsGzwwsLwLmpwMDw")))
    }

    @Test
    fun toPriorityValueExamples() {
        assertEquals(1, toPriorityValue('a'))
        assertEquals(26, toPriorityValue('z'))
        assertEquals(27, toPriorityValue('A'))
        assertEquals(52, toPriorityValue('Z'))
    }

    @Test
    fun groupByLogic() {
        val l = listOf("a", "b", "c", "d", "e", "f")
        val result = groupsOf(l, 3)

        assertEquals(listOf("a", "b", "c"), result[0])
        assertEquals(listOf("d", "e", "f"), result[1])
    }

    @Test
    fun example1is157() {
        val input = Reader().readResource("/2022/day_03_example.txt")

        val result = input.map { splitInHalf(it) }
            .map { findCommonLetter(it) }
            .sumOf { toPriorityValue(it) }

        assertEquals(157, result)
    }

    @Test
    fun examplePart2is70() {
        val input = Reader().readResource("/2022/day_03_example.txt")

        val result = groupsOf(input, 3)
            .map { findCommonLetter(it) }
            .sumOf { toPriorityValue(it) }

        assertEquals(70, result)
    }
}